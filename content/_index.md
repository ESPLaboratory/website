---
title: "Home"
---

# Qui sommes-nous ?
Nous sommes un groupe d'étudiants de différentes filières et différents niveaux déterminés à révolutionner la technologie de demain, dans une société coopérative. 


# Nos objectifs :

Dans un premier temps, nous avons pour but de concilier et regrouper toutes les connaissances et compétences, au sein des étudiants mais aussi auprès des experts et ingénieurs après une certaine expansion, allant de la cybersécurité au droit tout en passant par le  développement logiciel. De ce fait, les étudiants pourront s'entraider afin de tous participer sur des projets communs qui nécessiteront la spécialisation de chacun, pour le secteur public comme privé, ce qui leur apportera plus de professionnalisation en parallèle à leurs études.

Dans un second temps, nous souhaiterions considérablement améliorer le lien entre les étudiants et les entreprises. Être une passerelle entre ces deux mondes serait un avantage pour les deux camps. D'une part, les étudiants auront un accès direct aux entreprises qui proposeront des offres d'emploi ou seraient ouvertes aux candidatures spontanées, et d'autre part, les entreprises pourront recruter de jeunes talents bien plus vite que sur quelconque plate-forme tout en ayant la possibilité de proposer des misions ponctuelles.


